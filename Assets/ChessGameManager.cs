﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessGameManager : MonoBehaviour
{
    public delegate void InCheck(GameObject kingInCheck);
    public event InCheck inCheck = delegate { };

    public delegate void IsStalemate();
    public event IsStalemate isStalemate = delegate { };

    public delegate void IsCheckmate();
    public event IsCheckmate isCheckmate = delegate { };

    public GameObject kingOne;
    public GameObject kingTwo;

    GameObject kingInCheck;


    bool check;
    bool stalemate;
    bool checkmate;
    bool playerOneWins;
    bool playerTwoWins;
    enum GameStates
    {
        EGS_NormalPlay,
        EGS_InCheck,
        EGS_Stalemate,
        EGS_Checkmate,
        kingOneWins,
        kingTwowins,
        GameOver
    }

    GameStates currentGameState = GameStates.EGS_NormalPlay;

    void Start()
    {
        check = false;
        stalemate = false;
        checkmate = false;
        playerOneWins = false;
        playerTwoWins = false;
    }

    private void Update()
    {
        if(!kingOne || !kingTwo)
        {
            Debug.LogWarning("Missing muthufukn kings");
            return;
        }

        switch(currentGameState)
        {
            case GameStates.EGS_NormalPlay:
                NormalPlayUpdate();
                break;
            case GameStates.EGS_InCheck:
                Check();
                break;
            case GameStates.EGS_Stalemate:
                Stalemate();
                break;
            case GameStates.EGS_Checkmate:
                Checkmate();
                break;
            case GameStates.kingOneWins:
                KingOneWins();
                break;
            case GameStates.kingTwowins:
                KingTwoWins();
                break;
            case GameStates.GameOver:
                GameOver();
                break;
        }
    }

    void NormalPlayUpdate()
    {
        check = false;
        currentGameState = GameStates.EGS_NormalPlay;
        if (check && !checkmate)
        {
            Check();
        }
        if (stalemate)
        {
            Stalemate();
        }
        if (checkmate)
        {
            Checkmate();
        }
    }

    void Check()
    {
    //    if (!check)
    //    {
    //        check = true;
    //        currentGameState = GameStates.EGS_InCheck;
    //        inCheck(kingInCheck);
    //        if (/*king moves out of check || checking piece is captured || piece blocks check*/)
    //        {
    //            NormalPlayUpdate();
    //        }
    //    }
    }

    void Stalemate()
    {
        if (!stalemate)
        {
            stalemate = true;
            currentGameState = GameStates.EGS_Stalemate;
            isStalemate();
        }
    }

    void Checkmate()
    {
        if (!checkmate)
        {
            checkmate = true;
            currentGameState = GameStates.EGS_Checkmate;
            isCheckmate();
            if (playerOneWins)
            {
                KingOneWins();
            }
            else if(playerTwoWins)
            {
                KingTwoWins();
            }
        }
    }

    void KingOneWins()
    {
        playerOneWins = true;
        currentGameState = GameStates.kingOneWins;
        //UI should show saying player 1 won
        GameOver();
    }

    void KingTwoWins()
    {
        playerTwoWins = true;
        currentGameState = GameStates.kingTwowins;
        //UI should show saying AI won || player 2 won
        GameOver();
    }

    void GameOver()
    {
        // possibly use a delegate
        // UI should come up to either quit or rematch
        // if (player 1 or player 2 quits go back to main menu)
        // if (player 1 and player 2 rematch start new game)       
    }
}
