﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    public static SceneController scenes;

    Scene currentScene;

    public GameObject startMenu;
    public GameObject creditsUI;

    public float timer = 5f;

    float currentTimePassed;

    void Start()
    {
        if (scenes == null)
        {
            scenes = this;
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(startMenu);
            DontDestroyOnLoad(creditsUI);
        }
        startMenu.SetActive(false);
        creditsUI.SetActive(false);
        currentTimePassed = 0;
    }

    void Update()
    {
        currentScene = SceneManager.GetActiveScene();
        if (currentScene.name == "TitleScreen" && Input.GetKeyDown(KeyCode.Mouse0) || Input.GetKeyDown(KeyCode.Space))
        {
            SFXManager.Play("Menu Choice");
            startMenu.SetActive(true);
        }

        if (startMenu.activeSelf == true)
        {
            creditsUI.SetActive(false);
            currentTimePassed += 1 * Time.deltaTime;
            if (currentTimePassed >= timer)
            {
                startMenu.SetActive(false);
                currentTimePassed = 0;
            }
        }

        if (creditsUI.activeSelf == true)
        {
            currentTimePassed += 1 * Time.deltaTime;
            if (currentTimePassed >= timer)
            {
                creditsUI.SetActive(false);
                currentTimePassed = 0;
                startMenu.SetActive(true);
            }
        }
        EscapeQuit();
    }

    public void LoadSlipping()
    {
        SFXManager.Play("Menu Choice");
        startMenu.SetActive(false);
        currentTimePassed = 0;
        SceneManager.LoadScene("Slipping");
        
    }

    public void LoadLostCause()
    {
        SFXManager.Play("Menu Choice");
        SceneManager.LoadScene("LostCause");
        startMenu.SetActive(false);
        currentTimePassed = 0;
    }

    public void LoadRollingDeep()
    {
        SFXManager.Play("Menu Choice");
        SceneManager.LoadScene("RollingDeep");
        startMenu.SetActive(false);
        currentTimePassed = 0;
    }

    public void Credits()
    {
        SFXManager.Play("Menu Choice");
        startMenu.SetActive(false);
        creditsUI.SetActive(true);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void EscapeQuit()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Debug.Log("Quit");
            Application.Quit();
        }
    }
}
