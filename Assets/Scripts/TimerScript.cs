﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerScript : MonoBehaviour
{
    public static TimerScript instance;
    
    public Text timeDisplay;
    public Text loserText;

    public float timerCount = 60;
    public float waitTime = 5;
    void Start()
    {
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            return;
        }
    }

    void Update()
    {
        //Game timer counts down
        timerCount -= 1 * Time.deltaTime;
        if (timerCount <= 0)
        {
            timerCount = 0;
            GameOver();
        }
        timeDisplay.text = Mathf.RoundToInt(timerCount).ToString();
    }

    void GameOver()
    {
        StartCoroutine(LoadTitlescreen());
    }

    IEnumerator LoadTitlescreen()
    {
        loserText.text = "You Lost Your Way";
        yield return new WaitForSeconds(waitTime);
        loserText.text = "";
        SceneManager.LoadScene("TitleScreen");
    }
}
