﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class pauseController : MonoBehaviour
{
    GameObject lastSelection;
    public GameObject pauseMenu;
    bool paused;
    void Start()
    {
        Playing();
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab))
        {
            paused = paused ? false : true;
            {
                if (paused)
                {
                    Paused();
                }
                else
                {
                    Playing();
                }
            }
        }
    }

    void Paused()
    {
        pauseMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void SelectionChanged()
    {
        GameObject g = EventSystem.current.currentSelectedGameObject;
        lastSelection = g;
    }

    public void Playing()
    {
        pauseMenu.SetActive(false);
        Time.timeScale = 1;
    }

    public void ExitToMainMenu()
    {
        SceneManager.LoadScene("TitleScreen");
    }
}
