﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    GameObject followObject;

    public Vector3 offset;
    void Start()
    {
        if (followObject == null)
        {
            followObject = PlayerMove.instance.gameObject;
        }
    }

    void FixedUpdate()
    {
        if (followObject == null)
        {
            followObject = PlayerMove.instance.gameObject;
        }
        transform.position = followObject.transform.position + offset;
    }
}
