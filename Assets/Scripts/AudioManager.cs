﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    public AudioClip[] clips;
    public int maxSources = 5;

    private static AudioSource[] sources;
    private static int index = 0;

    private static AudioSource mainMusic;
    private static AudioSource bossMusic;

    public AudioClip mainMusicClip;
    public AudioClip bossMusicClip;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }

        sources = new AudioSource[maxSources];

        for (int i = 0; i < maxSources; i++)
        {
            GameObject g = new GameObject("SFX" + i);
            AudioSource s = g.AddComponent<AudioSource>();
            sources[i] = s;
            s.transform.parent = gameObject.transform;
        }

        GameObject mm = new GameObject("MainMusic");
        AudioSource mms = mm.AddComponent<AudioSource>();
        mainMusic = mms;
        mms.transform.parent = gameObject.transform;

        GameObject bm = new GameObject("Boss Music");
        AudioSource bms = bm.AddComponent<AudioSource>();
        bossMusic = bms;
        bms.transform.parent = gameObject.transform;
    }
    
    public static void Play(string clipName)
    {
        AudioClip clip = null;

        for (int i = 0; i < instance.clips.Length; i++)
        {
            if (instance.clips[i].name == clipName)
            {
                clip = instance.clips[i];
                break;
            }
        }

        if (clip != null)
        {
            sources[index].clip = clip;
            sources[index].Play();
            index = (index + 1) % instance.maxSources;
        }
    }

    public static void PlayMainMusic()
    {
        if (mainMusic.isPlaying) return;
        mainMusic.clip = instance.mainMusicClip;
        if(bossMusic.isPlaying)
        {
            bossMusic.Stop();
        }
        mainMusic.Play();
    }

    public static void PlayBossMusic()
    {
        if (bossMusic.isPlaying) return;
        bossMusic.clip = instance.bossMusicClip;
        if (mainMusic.isPlaying)
        {
            mainMusic.Stop();
        }
        bossMusic.Play();
    }
}

