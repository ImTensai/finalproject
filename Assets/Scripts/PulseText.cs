﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PulseText : MonoBehaviour
{
    public Text text;
    Color textColor;
    public float fadeDuration;

    private void Start()
    {
        textColor = text.color;
        StartCoroutine(Fade());
    }
   
    public IEnumerator Fade()
    {
        while (enabled)
        {
            for (float time = 0; time < fadeDuration; time += Time.deltaTime)
            {
                float frac = time / fadeDuration;
                text.color = Color.Lerp(textColor, new Color(1, 1, 1, 0), frac);
                yield return new WaitForEndOfFrame();
            }
            for (float time = 0; time < fadeDuration; time += Time.deltaTime)
            {
                float frac = time / fadeDuration;
                text.color = Color.Lerp(new Color(1, 1, 1, 0), textColor, frac);
                yield return new WaitForEndOfFrame();
            }
        }
    }
}
