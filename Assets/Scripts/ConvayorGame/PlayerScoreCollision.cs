﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScoreCollision : MonoBehaviour
{
    //Put on player

    [SerializeField] Text scoreText;
    [SerializeField] Text multiplierText;

    int score = 0;
    int addPoints = 150;
    int multiplier = 1;
    private void Start()
    {
        score = 0;
        multiplier = 1;
    }

    private void Update()
    {
        //When time gets below 30secs 2x multiplier
        if (TimerScript.instance.timerCount <= 30 && TimerScript.instance.timerCount > 15)
        {
            multiplier = 2;
        }
        //when time gets below 15sces 3x multiplier
        if (TimerScript.instance.timerCount <= 15)
        {
            multiplier = 3;
        }
        scoreText.text = score.ToString();
        multiplierText.text = "X" + multiplier.ToString();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("goodObject"))
        {
            ScorePoints();
        }

        if(collision.gameObject.CompareTag("badObject"))
        {
            ResetPoints();
        }
    }

    public int ScorePoints()
    {
        score = score + addPoints * multiplier;
        return score;
    }

    public int ResetPoints()
    {
        score = 0;
        return score;
    }
}
