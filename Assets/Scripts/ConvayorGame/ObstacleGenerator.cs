﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleGenerator : MonoBehaviour
{
    [SerializeField] float waitTime = 3f;
    [SerializeField] List<GameObject> obstacles;
    [SerializeField] List<Vector3> spawnPositions;

    private void Start()
    {
        StartCoroutine(SpawnObjects());
    }

    public IEnumerator SpawnObjects()
    {
        while (enabled)
        {
            //Randomly choose position and object to spawn
            Vector3 spawnPosition = spawnPositions[Random.Range(0, spawnPositions.Count)];
            GameObject chosenObstacle = obstacles[Random.Range(0, obstacles.Count)];

            //Set position and spawn object
            Vector3 pos = spawnPosition;
            GameObject g = Instantiate(chosenObstacle, pos, Quaternion.identity);
            yield return new WaitForSeconds(waitTime);
        }
    }
}
