﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Rigidbody body;

    Vector3 currentPos;
    Vector3 dir = Vector3.right;

    public float speed = 5f;

    void Start()
    {
        body = GetComponent<Rigidbody>();
        currentPos = body.transform.position;
    }

    void Update()
    {
        if(TimerScript.instance.timerCount <= 0)
        {
            body.transform.position = Vector3.zero;
        }
        else
        {
            //MoveLeft
            if (Input.GetKeyDown(KeyCode.A))
            {
                body.velocity = -dir * speed;
            }
            //MoveRight
            else if (Input.GetKeyDown(KeyCode.D))
            {
                body.velocity = dir * speed;
            }
        }
        transform.position = transform.position;
    }
}