﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMovementScript : MonoBehaviour
{
    Rigidbody body;

    float speed = 40f;

    Vector3 pos;
    Vector3 dir = new Vector3 (0,0,-1);

    private void Start()
    {
        body = GetComponent<Rigidbody>();
        pos = body.transform.position;
    }

    private void Update()
    {
        body.velocity = dir * speed;
    }
}
