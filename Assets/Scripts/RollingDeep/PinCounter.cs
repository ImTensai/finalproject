﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PinCounter : MonoBehaviour
{
    //this script goes on the pins in the scene
    public static PinCounter counter;

    public Text scoreTextNumber;
    public Text highScoreTextNumber;

    bool allPinsDown;

    static int score;
    int highScore;

    public Vector3 startPos;
    
    void Start()
    {
        score = 0;
        scoreTextNumber.text = "0";
        highScoreTextNumber.text = "0";
        startPos = transform.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("Ground") && !allPinsDown)
        {
            score++;
            Debug.Log(score);
            scoreTextNumber.text = score.ToString();

            if (score > highScore)
            {
                highScore = score;
                highScoreTextNumber.text = highScore.ToString();
            }
        }
    }
}
