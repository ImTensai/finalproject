﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class BowlingCamera : MonoBehaviour
{
    //scritp goes on the main camera

    public GameObject followObject;
    public Vector3 offset;
    private void Start()
    {
        transform.position = followObject.transform.position;
    }
    void Update()
    {
        transform.position = followObject.transform.position + offset;
    }
}
