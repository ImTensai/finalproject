﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPins : MonoBehaviour
{
    //goes on the pins
    public GameObject pinsParent;

    Transform pinsStartTrans;

    public float waitTime = 5;
    void Start()
    {
        pinsStartTrans = GetComponentInChildren<Transform>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            Debug.Log("Player");
            StartCoroutine(ResetBall());
        }
    }

    IEnumerator ResetBall()
    {
        yield return new WaitForSeconds(waitTime);
        //reset children back to pinsStartTrans
        Debug.Log("pins reset");
        yield return new WaitForEndOfFrame();
    }
}
