﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallRoll : MonoBehaviour
{
    public float speed = 20;

    Rigidbody body;

    public static Vector3 startPos;

    private void Start()
    {
        startPos = new Vector3(Random.Range(-9, 9), 5, 0);
        transform.position = startPos;
        body = GetComponent<Rigidbody>();
    }

    void Update()
    {
        Vector3 v = body.velocity;
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");
        
        Vector3 dir = new Vector3(x, 0, z).normalized * speed;
        dir.y = v.y;
        body.AddForce(dir);
    }
}
