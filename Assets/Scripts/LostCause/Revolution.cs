﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Revolution : MonoBehaviour
{
    //Put on the Directional Light
    float speed = 10;

    void Update()
    {
        transform.RotateAround(Vector3.zero, Vector3.forward, speed * Time.deltaTime);
        float dot = Vector3.Dot(transform.forward, Vector3.down) * 0.5f + 0.5f;
        RenderSettings.ambientIntensity = dot;
    }
}
