﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public static PlayerMove instance;

    Rigidbody body;

    Vector3 startPosition;

    Vector3 right = new Vector3 (0, 0, -1);
    Vector3 left = new Vector3(0, 0, 1);
    Vector3 forward = new Vector3(1, 0, 0);
    Vector3 back = new Vector3(-1, 0, 0);
    
    public float maxMoveSpeed = 7;
    public float maxAcceleration = .1f;
    float accerlTime = 5;
    float accelTime = 0;

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
        body = GetComponent<Rigidbody>();
        startPosition = transform.position;
    }

    void Update()
    {
        //if body.velocity is greater than maxVelocity body.velocity equals maxVelocity
        MoveForward();
        MoveBackward();
        MoveRight();
        MoveLeft();
    }

    void MoveForward()
    {
        if (Input.GetKey(KeyCode.W))
        {
            accelTime += Time.deltaTime;
            Vector3 maxVelocity = forward * maxMoveSpeed;
            Vector3 velDiff = maxVelocity - body.velocity.normalized;
            if(velDiff.magnitude > maxAcceleration)
            {
                velDiff = Mathf.Lerp(0, maxAcceleration, accelTime / accerlTime) * forward;
            }
            body.AddForce(velDiff, ForceMode.VelocityChange);
        }
    }

    void MoveBackward()
    {
        if (Input.GetKey(KeyCode.S))
        {
            accelTime += Time.deltaTime;
            Vector3 maxVelocity = back * maxMoveSpeed;
            Vector3 velDiff = maxVelocity - body.velocity.normalized;
            if (velDiff.magnitude > maxAcceleration)
            {
                velDiff = Mathf.Lerp(0, maxAcceleration, accelTime / accerlTime) * back;
            }
            body.AddForce(velDiff, ForceMode.VelocityChange);
        }
    }

    void MoveRight()
    {
        if (Input.GetKey(KeyCode.D))
        {
            accelTime += Time.deltaTime;
            Vector3 maxVelocity = right * maxMoveSpeed;
            Vector3 velDiff = maxVelocity - body.velocity.normalized;
            if (velDiff.magnitude > maxAcceleration)
            {
                velDiff = Mathf.Lerp(0, maxAcceleration, accelTime / accerlTime) * right;
            }
            body.AddForce(velDiff, ForceMode.VelocityChange);
        }
    }

    void MoveLeft()
    {
        if (Input.GetKey(KeyCode.A))
        {
            accelTime += Time.deltaTime;
            Vector3 maxVelocity = left * maxMoveSpeed;
            Vector3 velDiff = maxVelocity - body.velocity.normalized;
            if (velDiff.magnitude > maxAcceleration)
            {
                velDiff = Mathf.Lerp(0, maxAcceleration, accelTime / accerlTime) * left;
            }
            body.AddForce(velDiff, ForceMode.VelocityChange);
        }
    }

    public void Respawn()
    {
        body.Sleep();
        transform.position = startPosition;
    }
}
